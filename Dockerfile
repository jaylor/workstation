FROM ubuntu:latest AS builder

# ------------------
# Install Packer
# ------------------
ARG PACKER_VERSION=1.7.10

RUN apt-get clean && apt-get update && apt-get install -y curl unzip git && \
    curl -L -O https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip && \
    unzip packer_${PACKER_VERSION}_linux_amd64.zip && \
    mv packer /usr/bin && \
    rm packer_${PACKER_VERSION}_linux_amd64.zip && \
    packer -version

# ------------------
# Install Vault
# ------------------
ARG VAULT_VERSION=1.5.2

RUN curl -L -O https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip vault_${VAULT_VERSION}_linux_amd64.zip && \
    mv vault /usr/bin && \
    rm vault_${VAULT_VERSION}_linux_amd64.zip && \
    vault -version

# ------------------
# Install govc
# ------------------
ARG GOVC_VERSION=0.23.0

RUN curl -L -O https://github.com/vmware/govmomi/releases/download/v${GOVC_VERSION}/govc_linux_amd64.gz && \
    gunzip govc_linux_amd64.gz && \
    mv govc_linux_amd64 /usr/bin/govc && \
    chmod 755 /usr/bin/govc && \
    govc version

# ------------------
# Install tfswitch
# ------------------
RUN curl -Lo tfswitch_install.sh https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh && \
    chmod +x ./tfswitch_install.sh && \
    ./tfswitch_install.sh -b /usr/local/bin && \
    tfswitch --version

# ------------------
# Install aws-iam-authenticator
# ------------------
ARG AWS_IAM_AUTHENTICATOR_VERSION=0.5.9

RUN curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v${AWS_IAM_AUTHENTICATOR_VERSION}/aws-iam-authenticator_${AWS_IAM_AUTHENTICATOR_VERSION}_linux_amd64 && \
    mv aws-iam-authenticator /usr/bin/aws-iam-authenticator && \
    chmod +x /usr/bin/aws-iam-authenticator && \
    aws-iam-authenticator version

# ------------------
# Install flux cli
# ------------------
RUN curl -Lo flux_install.sh https://fluxcd.io/install.sh && \
    chmod +x ./flux_install.sh && \
    ./flux_install.sh

# ------------------
# Install kubeseal
# ------------------
ARG KUBESEAL_VERSION=0.20.5

RUN curl -Lo kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz \
    https://github.com/bitnami-labs/sealed-secrets/releases/download/v${KUBESEAL_VERSION}/kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz && \
    tar -xvzf kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz kubeseal && \
    chmod +x kubeseal && \
    mv kubeseal /usr/local/bin/kubeseal

# ------------------
# Install eksctl and eksctl-anywhere
# ------------------
RUN curl -sLO "https://anywhere-assets.eks.amazonaws.com/releases/eks-a/1/artifacts/eks-a/v0.5.0/linux/eksctl-anywhere-v0.5.0-linux-amd64.tar.gz" && \
    tar -xzf eksctl-anywhere-v0.5.0-linux-amd64.tar.gz -C /usr/local/bin && \
    curl -sLO "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_linux_amd64.tar.gz" && \
    tar -xzf eksctl_linux_amd64.tar.gz -C /usr/local/bin

# ------------------
# Install kubent
# ------------------
ARG KUBENT_VERSION=0.7.0

RUN curl -sLO "https://github.com/doitintl/kube-no-trouble/releases/download/${KUBENT_VERSION}/kubent-${KUBENT_VERSION}-linux-amd64.tar.gz" && \
    tar -xzf kubent-${KUBENT_VERSION}-linux-amd64.tar.gz -C /usr/local/bin

# ------------------
# Install kubectx and kubens
# ------------------
RUN git clone https://github.com/ahmetb/kubectx && \
    mv kubectx/kubectx kubectx/kubens /usr/local/bin/ && \
    chmod +x /usr/local/bin/kubectx /usr/local/bin/kubens

# ------------------
# Install starship
# ------------------
RUN curl -sLO "https://github.com/starship/starship/releases/latest/download/starship-x86_64-unknown-linux-gnu.tar.gz" && \
    tar -xzf starship-x86_64-unknown-linux-gnu.tar.gz -C /usr/local/bin


# ------------------
# Install base
# ------------------
FROM ubuntu:latest

COPY entrypoint.sh /

# ------------------
# Install packages
# ------------------
COPY requirements.txt /tmp

RUN apt-get clean && apt-get update && apt-get install -y curl unzip locales sshpass git jq vim sudo apt-transport-https \
    ca-certificates uuid-runtime openconnect python3 python3-pkg-resources python3-distutils python3-pip tmux && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor | tee /etc/apt/trusted.gpg.d/kubernetes.gpg 1> /dev/null && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list && \
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /etc/apt/trusted.gpg.d/helm.gpg 1> /dev/null && \
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list && \
    curl https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub | gpg --dearmor | tee /etc/apt/trusted.gpg.d/prebuilt-mpr-archive-keyring.gpg 1> /dev/null && \
    echo "deb https://proget.makedeb.org prebuilt-mpr jammy" | tee /etc/apt/sources.list.d/prebuilt-mpr.list && \
    curl https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/trivy.gpg 1> /dev/null && \
    echo "deb https://aquasecurity.github.io/trivy-repo/deb jammy main" | tee -a /etc/apt/sources.list.d/trivy.list && \
    apt-get clean && apt-get update && apt-get install -y kubectl=1.26.3-00 inetutils-ping \
    git jq ca-certificates vim sudo uuid-runtime openconnect python3 python3-pkg-resources python3-distutils python3-pip \
    curl unzip locales sshpass silversearcher-ag helm glab trivy pinentry-tty bash-completion && \
    pip3 install -r /tmp/requirements.txt && \
    apt-mark hold kubelet kubeadm kubectl

# ------------------
# Install vmware-ovftool
# ------------------
COPY VMware-ovftool-4.4.0-16360108-lin.x86_64.bundle /tmp

RUN locale-gen en_US.UTF-8 && \
    /tmp/VMware-ovftool-4.4.0-16360108-lin.x86_64.bundle --console --required --eulas-agreed && \
    rm -f /tmp/VMware-ovftool-4.4.0-16360108-lin.x86_64.bundle

# ------------------
# Install saml2aws
# ------------------

COPY saml2aws /usr/local/bin

# ------------------
# Copy tools from builder
# ------------------

COPY --from=builder /usr/bin/packer /usr/bin
COPY --from=builder /usr/bin/vault /usr/bin
COPY --from=builder /usr/local/bin /usr/local/bin
COPY --from=builder /usr/bin/govc /usr/bin
COPY --from=builder /usr/bin/aws-iam-authenticator /usr/bin

ENTRYPOINT ["/entrypoint.sh"]
